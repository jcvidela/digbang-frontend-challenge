import * as React from "react";
import ReactDOM from "react-dom";
import Slider from "rc-slider";
import Swal from "sweetalert2";

import { Modal, Input, Alert, Button } from "./components";

import "rc-slider/assets/index.css";
import "./App.css";

const App = () => {
  const [totalAmount, setTotalAmount] = React.useState<number>(5000);
  const [paymentTerm, setPaymentTerm] = React.useState<number>(3);

  const setPaymentTermValue = (newValue: number) => {
    setPaymentTerm(newValue);
  };

  function setTotalAmountValue(newValue: number) {
    setTotalAmount(newValue);
  }

  function handleTotalAmountInputChange(newValue: number) {
    setTotalAmount(newValue);
  }

  function handleCreditButtonClick() {
    Swal.fire({
      icon: "success",
      title: "Listo! :)",
      text: "Tu solicitud fue procesada con éxito",
    });
  }

  function handleDetailInstallmentsButtonClick() {
    Swal.fire({
      title: "Detalle de las cuotas",
      html: '<div id="custom-modal"></div>',
      willOpen: () => {
        ReactDOM.render(
          <Modal paymentTerm={paymentTerm} totalAmount={totalAmount} />,
          document.getElementById("custom-modal")
        );
      },
    });
  }

  return (
    <>
      <h1>Simulá tu crédito</h1>

      <div>
        <div className="section-container">
          <p>MONTO TOTAL</p>

          <div>
            <p>$</p>
            <Input value={totalAmount} onChange={handleTotalAmountInputChange} />
          </div>
        </div>

        <Slider
          value={totalAmount}
          min={5000}
          max={50000}
          onChange={setTotalAmountValue}
        />

        <div className="section-container">
          <p>$ 5.000</p>

          <p>$ 50.000</p>
        </div>
      </div>

      <div style={{ marginBottom: 20 }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <p>PLAZO</p>
          <Input value={paymentTerm} readOnly />
        </div>

        <Slider
          value={paymentTerm}
          min={3}
          max={24}
          onChange={setPaymentTermValue}
        />

        <div className="section-container">
          <p>3</p>

          <p>24</p>
        </div>
      </div>

      <div className="section-detail-container">
        <Alert paymentTerm={paymentTerm} totalAmount={totalAmount} />

        <div>
          <Button variant="primary" onClick={handleCreditButtonClick} />
          <Button variant="secondary" onClick={handleDetailInstallmentsButtonClick} />
        </div>
      </div>
    </>
  );
};

export default App;
