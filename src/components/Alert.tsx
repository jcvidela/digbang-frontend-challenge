import * as React from "react";
import styled from 'styled-components';
import formatARS from "../helpers/formatARS";

interface IFixedInstallmentProps {
  totalAmount: number;
  paymentTerm: number;
}

const StyledFixedInstallment = styled.div`
  background-color: #003660;
  height: 75px;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

const Alert: React.FC<IFixedInstallmentProps> = ({
  totalAmount,
  paymentTerm,
}) => {
  return (
    <StyledFixedInstallment>
      <p>CUOTA FIJA POR MES:</p>
      <p style={{ fontSize: 40 }}>{formatARS(totalAmount / paymentTerm)}</p>
    </StyledFixedInstallment>
  );
};

export default Alert;
