import * as React from 'react';
import formatARS from '../helpers/formatARS';

interface IModalProps {
  paymentTerm: number;
  totalAmount: number;
}

const CustomModal: React.FC<IModalProps> = ({ paymentTerm, totalAmount }) => {
  const calculateInstallments = () => {
    const installmentValue = totalAmount / paymentTerm;
    const installments = [];

    for (let i = 1; i <= paymentTerm; i++) {
      installments.push({
        installmentNumber: i,
        installmentValue: installmentValue,
      });
    }

    return installments;
  };

  let installments = calculateInstallments();

  return (
    <div>
      {
        <ul>
          {installments.map((installment) => (
            <li key={installment.installmentNumber}>
              Installment {installment.installmentNumber}: {formatARS(installment.installmentValue)}
            </li>
          ))}
        </ul>
      }
    </div>
  );
};

export default CustomModal;
