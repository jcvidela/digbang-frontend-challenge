import React from 'react';
import styled, { css } from 'styled-components';

interface IStyledButtonProps {
  variant: 'primary' | 'secondary';
}

const StyledButton = styled.button<IStyledButtonProps>`
  color: #ffffff;
  width: 350px;
  ${props =>
    props.variant === 'primary' &&
    css`
      background-color: #00ad8c;
      font-size: 23px
    `}
  ${props =>
    props.variant === 'secondary' &&
    css`
      width: 200px;
      background-color: #00568f;
    `}
`;

interface ICustomButtonProps {
  variant: 'primary' | 'secondary';
  onClick: () => void;
}

const CustomButton: React.FC<ICustomButtonProps> = ({ variant, onClick }) => (
  <StyledButton variant={variant} onClick={onClick}>
    {variant === 'primary' ? 'OBTENER CRÉDITO' : 'VER DETALLE DE CUOTAS'}
  </StyledButton>
);

export default CustomButton;
