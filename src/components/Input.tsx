import * as React from "react";
import styled from "styled-components";

interface IAmountInputProps {
  value: number;
  onChange?: (value: number) => void;
  readOnly?: boolean;
}

const StyledInput = styled.input`
  background-color: #003c6a;
  border: 1px solid #ffffff;
  color: #ffffff;
  padding: 0;
  text-align: center;
  font-size: 25px;
  font-weight: 600;
  width: 150px;
  height: 40px;
`;

const CustomInput: React.FC<IAmountInputProps> = ({
  value,
  onChange = () => {},
  readOnly = false
}) => {
  const updateTotalAmountFromInput = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newValue = parseFloat(event.target.value);
    onChange(newValue);
  };

  return (
    <StyledInput readOnly={readOnly} value={value} onChange={updateTotalAmountFromInput} />
  );
};

export default CustomInput;
